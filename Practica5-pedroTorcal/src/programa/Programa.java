package programa;

import java.time.LocalDate;

import clases.Tienda;
/**
 * 
 * @author torre
 *
 */
public class Programa {

	public static void main(String[] args) {

		// 1- Crear instancia de GestorClases
		System.out.println("instancia Gestor de clases");
		Tienda miTienda = new Tienda();
		// 2- Dar de alta 3 elementos de Clase1 y listar
		miTienda.altaAplicacionInformatica("Word", "Aplicacion Ofimatica", "Windows 10", 1.5236, 129, LocalDate.now(),
				"docx");
		miTienda.altaAplicacionInformatica("Excel", "Aplicacion Ofimatica", "Windows 10", 1.5236, 129, LocalDate.now(),
				"XLSX");
		miTienda.altaAplicacionInformatica("PowerPoint", "Aplicacion Ofimatica", "Windows 10", 1.5236, 129,
				LocalDate.now(), "ppt");
		miTienda.listarAplicacionInformatica();

		// 3- Buscar un elemento de Clase1
		miTienda.buscarAplicacionInformatica("Word", 1.5236);
		// 4- Eliminar un elemento de Clase1 y listar
		miTienda.eliminarAplicacionInformatica("Word", 1.5236);
		miTienda.listarAplicacionInformatica();
		// 5- Dar de alta 3 elementos de Clase2 y listar
		miTienda.altaEmpresa("Microsoft", 902000001, "USA", "15236584");
		miTienda.altaEmpresa("Affinity Serif", 902515562, "USA", "128854798");
		miTienda.altaEmpresa("Apple", 902554783, "USA", "12587922");
		miTienda.listarEmpresaTodas();
		// 6- Buscar un elemento de Clase2
		miTienda.buscarEmpresa("Apple", "12587922");
		// 7- Eliminar un elemento de Clase2 y listar
		
		miTienda.eliminarEmpresa("Affinity Serif", "128854798");		
		miTienda.listarEmpresaTodas();
		// 8- Lista elementos Clase2 por alg�n atributo
		miTienda.listarEmpresa("15236584");
		// 9- Listar elementos Clase2 con un elemento de Clase1
		miTienda.listarEmpresaAplicacion("Apple", "Apple Music", 1.0);
		// 10- Asignar un elemento que use Clase1 y Clase2 y listar
		
		
		miTienda.listarEmpresaTodas();
	}

}
