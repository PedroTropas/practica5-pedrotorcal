package clases;

import java.time.LocalDate;

public class AplicacionInformatica {

	// Atributos
	private String nombre;
	private String descripcion;
	private String sistemaOperativo;
	private double version;
	private double precio;
	private LocalDate fecha;
	private String formatoArchivos;

	// constuctor vacio
	public AplicacionInformatica() {
		super();
	}

	// constructor con todos los atributos
	public AplicacionInformatica(String nombre, String descripcion, String sistemaOperativo, double version,
			double precio, LocalDate fecha, String formatoArchivos) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.sistemaOperativo = sistemaOperativo;
		this.version = version;
		this.precio = precio;
		this.fecha = fecha;
		this.formatoArchivos = formatoArchivos;
	}

	// Getters y Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSistemaOperativo() {
		return sistemaOperativo;
	}

	public void setSistemaOperativo(String sistemaOperativo) {
		this.sistemaOperativo = sistemaOperativo;
	}

	public double getVersion() {
		return version;
	}

	public void setVersion(double version) {
		this.version = version;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getFormatoArchivos() {
		return formatoArchivos;
	}

	public void setFormatoArchivos(String formatoArchivos) {
		this.formatoArchivos = formatoArchivos;
	}

	// toString
	@Override
	public String toString() {
		return "AplicacionInformatica [nombre=" + nombre + ", descripcion=" + descripcion + ", sistemaOperativo="
				+ sistemaOperativo + ", version=" + version + ", precio=" + precio + ", fecha=" + fecha
				+ ", formatoArchivos=" + formatoArchivos + "]";
	}

}
