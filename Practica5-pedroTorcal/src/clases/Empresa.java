package clases;

public class Empresa {
		//Atributos
		private String nombre;
		private int telefono;
		private String direccion;
		private String cif;
		
		
		//constructor vacio
		public Empresa() {
			super();
		}
		//constructor con todos los atributos
		public Empresa(String nombre, int telefono, String direccion, String cif) {
			super();
			this.nombre = nombre;
			this.telefono = telefono;
			this.direccion = direccion;
			this.cif = cif;
		}
		//Getters y Setters
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public int getTelefono() {
			return telefono;
		}
		public void setTelefono(int telefono) {
			this.telefono = telefono;
		}
		public String getDireccion() {
			return direccion;
		}
		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}
		public String getCif() {
			return cif;
		}
		public void setCif(String cif) {
			this.cif = cif;
		}
		
		//toString
		@Override
		public String toString() {
			return "Empresa [nombre=" + nombre + ", telefono=" + telefono + ", direccion=" + direccion + ", cif=" + cif
					+ "]";
		}
		
		
}
