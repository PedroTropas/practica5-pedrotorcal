package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

import clases.AplicacionInformatica;
import clases.Empresa;

public class Tienda {

	// Atributos
	private ArrayList<AplicacionInformatica> listaAplicacionInformatica;
	private ArrayList<Empresa> listaEmpresa;

	// Constructor
	public Tienda() {
		listaAplicacionInformatica = new ArrayList<AplicacionInformatica>();
		listaEmpresa = new ArrayList<Empresa>();
	}

	// M�todo alta Clase1 (comprobando previamente si existe)
	public void altaAplicacionInformatica(String nombre, String descripcion, String sistemaOperativo, double version,
			double precio, LocalDate fecha, String formatoArchivos) {
		// buscarAplicacionInformatica.equals
		if (buscarAplicacionInformatica(nombre, version).equals(nombre)
				&& buscarAplicacionInformatica(nombre, version).equals(version)) {
			AplicacionInformatica nuevaAplicacionInformatica = new AplicacionInformatica(nombre, descripcion,
					sistemaOperativo, version, precio, fecha, formatoArchivos);
			listaAplicacionInformatica.add(nuevaAplicacionInformatica);
		} else {
			System.out.println("El Pragrama introducido ya existe");
		}
	}

	// M�todo listar Clase1
	public void listarAplicacionInformatica() {
		for (AplicacionInformatica aplicacionInformatica : listaAplicacionInformatica) {
			if (aplicacionInformatica != null) {
				System.out.println(aplicacionInformatica);
			}
		}
	}

	// M�todo buscar elemento Clase1
	public AplicacionInformatica buscarAplicacionInformatica(String nombre, double version) {
		for (AplicacionInformatica aplicacionInformatica : listaAplicacionInformatica) {
			if (aplicacionInformatica != null && aplicacionInformatica.getNombre().equals(nombre)
					&& aplicacionInformatica.getVersion() == version) {
				return aplicacionInformatica;
			}

		}
		return null;
	}
	// M�todo eliminar Clase1

	public void eliminarAplicacionInformatica(String nombre, double version) {
		Iterator<AplicacionInformatica> iteradorAplicacionInformatica = listaAplicacionInformatica.iterator();
		while (iteradorAplicacionInformatica.hasNext()) {
			AplicacionInformatica aplicacionInformatica = (AplicacionInformatica) iteradorAplicacionInformatica.next();
			if (aplicacionInformatica.getNombre().equals(nombre) && aplicacionInformatica.getVersion() == version) {
				iteradorAplicacionInformatica.remove();
			}
		}
	}
	// M�todo alta Clase2 (comprobando previamente si existe)
	public void altaEmpresa( String nombre,int telefono,String direccion,String cif) {
		// buscarAplicacionInformatica.equals
		if (buscarEmpresa(nombre, cif).equals(nombre)
				&& buscarEmpresa(nombre, cif).equals(cif)) {
			Empresa nuevaEmpresa = new Empresa(nombre, telefono,
					direccion, cif);
			listaEmpresa.add(nuevaEmpresa);
		} else {
			System.out.println("La Empresa introducida ya existe");
		}
	}
	
	
	// M�todo eliminar Clase2

	public void eliminarEmpresa(String nombre, String cif) {
		Iterator<Empresa> iteradorEmpresa = listaEmpresa.iterator();
		while (iteradorEmpresa.hasNext()) {
			Empresa empresa = (Empresa) iteradorEmpresa.next();
			if (empresa.getNombre().equals(nombre) && empresa.getCif() == cif) {
				iteradorEmpresa.remove();
			}
		}
	}
	// M�todo buscar elemento Clase2
	public Empresa buscarEmpresa(String nombre, String cif) {
		for (Empresa empresa : listaEmpresa) {
			if (empresa != null && empresa.getNombre().equals(nombre)
					&& empresa.getCif() == cif) {
				return empresa;
			}

		}
		return null;
	}
	
	// M�todo listar Clase2 
		public void listarEmpresaTodas() {
			for (Empresa empresa : listaEmpresa) {
				if (empresa != null) {
					System.out.println(empresa);
				}
			}
		}
	
	
	
	
	// M�todo listar Clase2 por alg�n atributo
	public void listarEmpresa(String cif) {
		for (Empresa empresa : listaEmpresa) {
			if (empresa != null&& empresa.getCif().equals(cif)) {
				System.out.println(empresa);
			}
		}
	}
	// M�todo listar elementos Clase2 con un elemento Clase1
	public void listarEmpresaAplicacion(String nombreEmpresa, String nombreAplicacion, double version) {
		for (Empresa empresa : listaEmpresa) {
			if (empresa != null&& empresa.getNombre().equals(nombreEmpresa)){
				for (AplicacionInformatica aplicacionInformatica : listaAplicacionInformatica) {
					if(aplicacionInformatica!=null&&aplicacionInformatica.getNombre().equals(nombreAplicacion)&&aplicacionInformatica.getVersion()==version) {
						System.out.println(empresa);
					}
				}
			}else {System.out.println("No coinciden los datos aportados");}
		}
	}
	// M�todo asignar elemento Clase1 a Clase2
	
}
